
from statistiques import enrichir_rangs, histogramme
import math
import datetime
import tech
from bdd import sans_accent
import csv
import os

def codeheure() :
    return datetime.datetime.now().strftime("%Y%m%d%H%M%S")

class LaTeXRapport(tech.FileLaTeX, tech.MemLaTeX) :
    
    preambule_def=r"""\pagestyle{empty}
    \usepackage[lighttt]{lmodern}
    \usepackage{alltt}
    \usepackage[nott]{kpfonts}"""
    
    def __init__(self, **kwargs) :
        if kwargs.get("basename", None) is None :
            tech.MemLaTeX.__init__(self)
        else :
            tech.FileLaTeX.__init__(self, kwargs["basename"])
        if "debut" in kwargs :
            print(kwargs["debut"], file=self)
        else :
            print(r"""\documentclass[a4paper,10pt, french, DIV=16,"""+ ("twoside=semi" if kwargs.get("twoside", False) else "")+ r"""]{scrartcl}
            \usepackage[french]{babel}
            \usepackage{t1enc}
            \usepackage{longtable}
            \setlength\LTpre{0pt}
            \setlength\LTpost{.5ex}
            \usepackage{listings}
            \usepackage{csquotes}
            \lstnewenvironment{lstsql}{\lstset{language=sql, breaklines=true, breakatwhitespace=true, breakautoindent=false, breakindent=0pt, basicstyle=\ttfamily}}{}
            \usepackage{pgfplots}
            \usepackage{pdfescape}
            \usepackage{bookmark}
            \def\mybookmark#1#2#3{\EdefEscapeName\Target{#3}\hypertarget{\Target}{}\bookmark[dest={\Target},level={#1}]{#2}}""", file=self)
            if "nf_preambule" in kwargs :
                super().inclure_preambule(kwargs["nf_preambule"], LaTeXRapport.preambule_def)
        self._bookmarks = kwargs.get("bookmarks", [])
    
    def bookmark(self, level, name) :
        self._bookmarks = self._bookmarks[:level]
        self._bookmarks.extend([""] * (level - len(self._bookmarks)))
        self._bookmarks.append(sans_accent(name))
        target = r"§\#§".join(self._bookmarks)
        print(r"\mybookmark{", level, "}{", name, "}{", target, "}", sep='', file=self)
    
    def finaliser(self, compiler, montrer, wait=True) :
        """Termine le .tex, le ferme, et éventuellement le compile et affiche le pdf"""
        super().finaliser()
        if compiler :
            self._runLaTeX(wait)
            if montrer :
                self._showPDF()
    
###Production de rapports, fiches, etc.


## Rapport

def titre(s, ltx) :
    print(r"\begin{center}\Large ", s, r"\end{center}", file=ltx)
    
def recup_donnees(bdd, table_ds) :
    qcm = len(bdd.execute("select * from sqlite_master where type=? and name=?", ("table", table_ds+"_qcm")).fetchall()) > 0
    les_classes = [x[0] for x in bdd.execute("select distinct classe from " + table_ds + "_notes ORDER BY classe ASC").fetchall()]
    donnees = [dict(x) for x in bdd.execute("select classe, groupe, option, nom, prenom, note, STRROUND(note, 2) AS dnote, email, commentaire from " + table_ds + "_notes order by note desc, nom collate ordonner_noms, prenom collate ordonner_noms;").fetchall()]
    enrichir_rangs(donnees, "note", None, "rang")
    enrichir_rangs(donnees, "note", "classe", "rang_classe")
    cles = ["classe", "groupe", "option", "nom", "prenom", "dnote", "rang", "rang_classe"]
    #histogramme
    cases = ["{[" + str(2*n) + "," + str(2*(n+1)) +"[}" for n in range(10)]
    cases.append(r"{\strut20+}")
    histo_gen = histogramme(donnees, "note", None, lambda n : max(0, int(math.floor(n))//2) if n<20 else 10, 11)
    histo_classes = histogramme(donnees, "note", "classe", lambda n : max(0, int(math.floor(n))//2) if n<20 else 10, 11)
    return les_classes, donnees, cles, cases, histo_gen, histo_classes, qcm

def Rapport(args) :
    """Génère un rapport avec des stats, des classements, etc."""
    table_ds,table_etu, bdd, nf_preambule = args.tbl_ds, args.tbl_etu, args.bdd, args.preambule
    ltx = LaTeXRapport(basename = table_ds+"_"+codeheure()+"/rapport_"+table_ds, nf_preambule = nf_preambule)
    les_classes, donnees, cles, cases, histo_gen, histo_classes, qcm = recup_donnees(bdd, table_ds)
    # stats générale,s à suivre
    ltx.bookmark(0, "Généralités")
    ltx.bookmark(1, "Statistiques")
    titre("Statistiques", ltx)
    for req in ["""select classe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + "_notes group by classe;", """select classe, groupe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + "_notes group by classe, groupe;"] :
        ltx.write_sql(bdd, req, align="r")
        print(r"\bigskip", file=ltx)
    
    
    #ltx.write_table(range(21), [histo_gen])
    if len(les_classes) > 1 :
        ltx.write_histo(cases, [histo_gen]+[histo_classes[x] for x in sorted(histo_classes.keys())], titres = ["Global"] + sorted(histo_classes.keys()), align="r")
        print(r"\bigskip", file=ltx)


    
        ltx.write_sql(bdd, """select question, item,  STRROUND(points,2) AS "barème",Répondu, CAST(100*Min/bareme AS int)||" \%" AS "Réussite mini", CAST(100*Max/bareme AS int)||" \%" AS "Maxi", CAST(100*Moy/bareme AS int)||" \%" AS "Moy", CAST(100*ect/bareme AS int)||" pt\%" AS "Écart-type" from (SELECT question, item, bareme, points FROM """ + table_ds + """_struct) as s NATURAL LEFT JOIN (select question, item, COUNT(*) AS "Répondu", STRROUND(MIN(points),2) AS "Min", STRROUND(MAX(points),2) AS "Max", STRROUND(AVG(points),2) AS "Moy", STRROUND(STDEV(points),2) AS "ect" from """ + table_ds + "_result GROUP BY question, item) ORDER BY question COLLATE ORDONNER_QUEST, item;", align="r")
    
    
    print(r"\clearpage", file=ltx)
    # notes
    if len(les_classes) > 1 :
        ltx.bookmark(1, "Classement général")
        titre("Classement général", ltx)
        ltx.write_table(cles, donnees, align="c")
        print(r"\clearpage", file=ltx)
    for classe in les_classes :
        donnees_classe = [eleve for eleve in donnees if eleve["classe"] == classe]
        ltx.bookmark(0, classe)
        ltx.bookmark(1, "Classement")
        titre(classe + " --- Classement", ltx)
        print(r"\bigskip", file=ltx)
        ltx.write_table(cles, donnees_classe, align="c")
        print(r"\clearpage", file=ltx)
        ltx.bookmark(1, "Notes")
        titre(classe + " --- Notes", ltx)
        donnees_classe.sort(key=lambda x : (sans_accent(x["nom"]), sans_accent(x["prenom"])))
        ltx.write_table(cles, donnees_classe, align="c")
        print(r"\clearpage", file=ltx)
        ltx.bookmark(1, "Statistiques")
        titre(classe + " --- Statistiques", ltx)
        ltx.write_histo(cases, [histo_classes[classe]], align="c", hauteur=3.5)
        ltx.write_sql(bdd, """select question, item, STRROUND(points,2) AS "barème", Répondu, CAST(100*Min/bareme AS int)||" \%" AS "Réussite mini", CAST(100*Max/bareme AS int)||" \%" AS "Maxi", CAST(100*Moy/bareme AS int)||" \%" AS "Moy", CAST(100*ect/bareme AS int)||" pt\%" AS "Écart-type" from (SELECT question, item, bareme, points FROM """ + table_ds + """_struct) NATURAL LEFT JOIN (select question, item, COUNT(*) AS "Répondu", STRROUND(MIN(points),2) AS "Min", STRROUND(MAX(points),2) AS "Max", STRROUND(AVG(points),2) AS "Moy", STRROUND(STDEV(points),2) AS "ect" from (SELECT * FROM """ + table_ds + "_result NATURAL JOIN " + table_etu+ " WHERE classe=?) GROUP BY question, item) ORDER BY question COLLATE ORDONNER_QUEST, item;", param=(classe,), align="c")

        print(r"\clearpage", file=ltx)
        ltx.bookmark(1, "Commentaires")
        titre(classe + " --- Commentaires", ltx)
        for x in donnees_classe :
            if x["commentaire"] is not None :
                 print(r"""\paragraph{""", x["nom"], x["prenom"], r"""("""+ str(x["dnote"]) + r""")} """, file=ltx)
                 ltx.bookmark(2, x["nom"] + " " + x["prenom"])
                 print(x["commentaire"], r"""\par""", file=ltx)
        print(r"\clearpage", file=ltx)
    bdd.close()
    ltx.finaliser(True, True)


    
    
## Fiches individuelles
def Fiches(args) :
    """Génère une fiche pour chaque élève. On peut restreindre à une liste de classes et/ou de groupes. On peut choisir d'afficher des statistiques par groupe"""
    table_ds, table_etu, bdd, nf_preambule, classes, groupe, stats_par_groupes, elaguer, indiv = args.tbl_ds, args.tbl_etu, args.bdd, args.preambule, args.classe, args.groupe, args.stat_groupe, args.elaguer, args.indiv
    codeh = codeheure()
    basename = table_ds+"_"+codeh
    ltx = LaTeXRapport(basename = None if indiv else basename+"/fiches_"+table_ds, nf_preambule = nf_preambule, twoside = True)
    les_classes, donnees, cles, cases, histo_gen, histo_classes, qcm = recup_donnees(bdd, table_ds)
    print(r"""\newbox\boxstat
    \def\stats{\usebox\boxstat}
    \setbox\boxstat=\vbox{\dimendef\pagegoal=0 \pagegoal=0pt\small%""", file=ltx)
    if len(les_classes) > 1 :
         ltx.write_sql(bdd, """select count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + """_notes;""", align="r")
    ltx.write_sql(bdd, """select classe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + """_notes group by classe;""",align="r")
    if stats_par_groupes :
        ltx.write_sql(bdd, """select classe, groupe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + """_notes group by classe, groupe;""",align="r")
    if len(les_classes) == 1 :
            ltx.write_histo(cases, [histo_gen] , align="r")
    else :
            ltx.write_histo(cases,[histo_gen]+[histo_classes[x] for x in sorted(histo_classes.keys())] , titres = ["Global"] + sorted(histo_classes.keys()), align="r")
    print(r"""}""", file=ltx)
    eleves_notes = [dict(x) for x in bdd.execute("SELECT *, STRROUND(note, 2) AS dnote FROM " + table_ds + "_notes order by classe, nom collate ordonner_noms").fetchall()]
    enrichir_rangs(eleves_notes, "note", None, "rang")
    enrichir_rangs(eleves_notes, "note", "classe", "rang_classe")
    classe_courante = None
    fichCSV = None
    for eleve in eleves_notes :
        if (classes == [] or classes is None or eleve["classe"] in classes) and (groupe is None or eleve["groupe"] == groupe) and (not elaguer or eleve["note"] is not None) :
            if indiv :
                if eleve["classe"] != classe_courante :
                    classe_courante = eleve["classe"]
                    if fichCSV is not None :
                        fichCSV.close()
                    os.makedirs(os.path.join("/tmp", basename, eleve["classe"]), exist_ok=True)
                    fichCSV = open(os.path.join("/tmp", basename, eleve["classe"], "liste.csv"), "w", newline="")
                    listeCSV = csv.DictWriter(fichCSV, fieldnames=tuple(eleve.keys())+("pdf",), dialect="unix")
                    listeCSV.writeheader()
                fiche = LaTeXRapport(basename = basename+"/"+eleve["classe"]+"/"+str(eleve["id"]) + "-" + eleve["nom"].replace(" ", ""), debut = ltx.getvalue())
                fiche_eleve(fiche, bdd, table_ds, eleve, qcm)
                listeCSV.writerow({**eleve, "pdf":str(eleve["id"]) + "-" + eleve["nom"].replace(" ", "")+".pdf"})
                fiche.finaliser(True, False, True)
            else :
                if eleve["classe"] != classe_courante :
                    ltx.bookmark(0, eleve["classe"])
                    classe_courante = eleve["classe"]
                ltx.bookmark(1, eleve["nom"] + " "+ eleve["prenom"])
                fiche_eleve(ltx, bdd, table_ds, eleve, qcm)
                print(r"""\cleardoublepage""", file=ltx)
    if indiv :
        fichCSV.close()
    bdd.close()
    ltx.finaliser(not indiv, not indiv, not indiv)


    

def fiche_eleve(ltx, bdd, table_ds, eleve, qcm) :
    print(r"\begin{center}\LARGE " + eleve["nom"] + " " + eleve["prenom"] + r"\\", file=ltx)
    print(r"\large " + eleve["classe"] + r"\end{center}", file=ltx)
    print(r"\begingroup", file = ltx)
    print("Note :", str(eleve["dnote"]), r"/ 20\hfill", file=ltx)
    print("Rang dans la classe :", eleve["rang_classe"], r"\hfill", file=ltx)
    print("Rang global :", eleve["rang"], file=ltx)
    print(r"\endgroup", file=ltx)
    print(r"\stats", file=ltx)
    if eleve["commentaire"] is not None and eleve["commentaire"] != "" :
        print(r"\section*{Commentaire}\begingroup\small", eleve["commentaire"], r"\endgroup", file=ltx)
    print(r"\section*{Détail de votre copie}\begingroup\small", file=ltx)
    if qcm :
        print(r"\raisebox{0pt}[0pt][0pt]{\begin{minipage}[t]{\textwidth}", file=ltx)
        ltx.write_sql(bdd, """select qcm.quest as "QCM", qcm.corrige as "Corrigé", res.reponse as "Réponse", STRROUND(res.points, 1) as "Points", qcm.coeff AS "Max" from """ + table_ds +"""_qcm as qcm left join (select * from """ + table_ds + """_qcmrep where id=?) as res on qcm.quest=res.quest order by qcm.quest;""", (eleve["id"],), align="r")
        print(r"\end{minipage}}", file=ltx)
    ltx.write_sql(bdd, """select s.question, s.item, CAST(STRROUND(100*r.points/s.bareme, 0) as int)||" \%" as "Réussite", STRROUND(s.points,2) as "Barème", STRROUND(r.points*s.points/s.bareme,2) as "Vos points" from """+table_ds+"""_struct as s left join (select * from """  +table_ds + """_result where id=?) as r on s.question=r.question and s.item=r.item order by s.question collate ordonner_quest, s.rowid;""", (eleve["id"],))
    print(r"\endgroup", file=ltx)
