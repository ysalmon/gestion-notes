import csv

from moteur import PlusieursEleves, AucunEleve, MauvaisNbItems, ItemTropGrand

from interf_texte import *



def getitem_nofail(dict, key) :
    try :
        return dict[key]
    except KeyError :
        return None

def none_to_empty(x) :
    if x is None :
        return ""
    else :
        return x
        
class InterfaceCSV(InterfaceAuto) :
    
    def __init__(self, fich_base, table_etu, table_ds_bn, nom_fichier, action_doublon) :
        super().__init__(fich_base, table_etu, table_ds_bn, nom_fichier, action_doublon)
        dialect = csv.Sniffer().sniff(self.f.readline(), delimiters=",;")
        self.f.seek(0)
        self.csv = csv.DictReader(self.f, dialect=dialect)

    
    def TraiterCopie(self) :
        """Traite une ligne du fichier """
        try :
            copie = next(self.csv)
            if getitem_nofail(copie,"id") is None :
                print("Ligne sans id, ignorée")
                return
            eleve = self.ds.SelectionnerEleveParId(copie["id"])
        except UnicodeDecodeError :
            print("Erreur de décodage Unicode ; le fichier doit être en UTF8")
            raise SortieEleve
        except ValueError :
            print("Erreur de saisie")
            raise AbandonDebut
        except AucunEleve :
            print("Élève non trouvé (id =", copie["id"], ")")
            raise AbandonDebut
        except StopIteration :
            raise SortieEleve
        print("Copie de ", list(eleve))
        if len(self.ds.select_from_result("*", "WHERE id = ?", (eleve["id"],)).fetchall()) > 0 :
            action = self.QueFaireDoublon()
            if action is ActionDoublon.Ignorer :
                raise AbandonDebut
            if action is ActionDoublon.Ecraser :
                self.ds.EffacerToutesNotes(eleve["id"])
                self.ds.EffacerCommentaire(eleve["id"])
        for q_nb in range(0, len(self.ds.questions())) :
            question = self.ds.NomQuestion(q_nb)
            les_items = self.ds.ItemsQuestion(q_nb)
            saisie = "/".join([none_to_empty(getitem_nofail(copie,question + "/" + item[0])) for item in les_items])
            try :
                (replace, repq, note) = self.ParseNote(saisie)
            except ValueError :
                print("Erreur de saisie")
                raise AbandonMilieu(eleve["id"])
            if note is None :
                continue
            if note == ActionNote.Delete :
                try :
                    self.ds.EffacerNote(eleve["id"], question)
                except :
                    print("Impossible d'effacer la note pour la question", question)
                    raise AbandonMilieu(eleve["id"])
            try :
                self.InsereNote(eleve["id"], q_nb, note, replace)
            except MauvaisNbItems :
                print ("Mauvais nombre d'items pour la question " + question)
                raise AbandonMilieu(eleve["id"])
            except ItemTropGrand :
                print ("Valeur trop élevée (" + str(note) +") pour un item en question "+ question)
                raise AbandonMilieu(eleve["id"])
        
        self.ds.commit()
        self.NoteTotale(eleve)
        return
    
